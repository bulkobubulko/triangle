def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if a == b == c:
        return True
    elif a == b or a == c or b == c:
        return True
    elif a <= 0 or b <= 0 or c <= 0:
        return False
    elif a + b > c and a + c > b and b + c > a:
        return True
    else:
        return False
